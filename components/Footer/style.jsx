import styled from 'styled-components';

export default styled.footer`
  color: red;
  font-family: 'Times New Roman', Times, serif;
  position: fixed;
  bottom: 0;
  text-align: center;

  font-size: 3em;
  font-weight: bolder;
  padding: 2rem;
  margin: auto;
  width: 50%;
`;
