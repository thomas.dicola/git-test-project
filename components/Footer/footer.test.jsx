import { render } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';

import Footer from '.';

it('should contain the good string', () => {
  const component = render(<Footer />);
  const footerElement = component.getByText('FOOTER');

  expect(footerElement).toBeInTheDocument();
});
