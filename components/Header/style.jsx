import styled from 'styled-components';

export default styled.div`
  color: blueviolet;
  font-family: 'Times New Roman', Times, serif;
  position: fixed;
  text-align: center;
  width: 100%;
  font-size: 3em;
  font-weight: bolder;
`;
